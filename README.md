# flex-common

flex-common abstracts away implementations common to all flex microservices

### Making Private Library Work

To make this library which is hosted in a private repository works follow the below:

1. Ensure that the module name is same as the repository url path

    ```bash
    gitlab.com/flexkode/flex-common
    ```

2. Edit your `~/.ssh/config` to have your ssh keys configures approriately

    ```bash
    Host gitlab.com
        Preferredauthentications publickey
        IdentityFile ~/.ssh/iits_ekb_id_rsa
    ```

3. Make modifications to `~/.gitconfig` to have default `https://` changes to `git`

    ```bash
    [url "git@gitlab.com:"]
        insteadOf = https://gitlab.com/
    ```

4. Now get your library as you would with any other go library

    ```bash
    go get gitlab.com/flexkode/flex-common
    ```