package repository

import (
	"fmt"

	juggler "github.com/bekinsoft/ds-juggler"
	util "github.com/idealitsol/beacon-util"
	"github.com/jinzhu/gorm"
)

// Repository struct
type Repository struct {
	GormDB *gorm.DB
}

// Create Repository
func (r *Repository) Create(model interface{}, out interface{}) error {
	if err := r.GormDB.Create(model).First(out).Error; err != nil {
		return err
	}

	return nil
}

// Find Repository
//		model	= Struct
// 		filter 	=
func (r *Repository) Find(out interface{}, filter interface{}) (err error) {
	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	filterMap, err := getFilter(filter)
	if err != nil {
		return
	}

	tx, err = juggler.FilterQuery(filterMap, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	if err := tx.Find(out).Error; err != nil { // https://github.com/jinzhu/gorm/issues/857
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

// FindByID Repository
//		model	= Struct
//		id		= Primary Key
// 		filter 	= Map
func (r *Repository) FindByID(out interface{}, id interface{}, filter interface{}) error {
	// Get filter map
	filterMap, err := getFilter(filter)
	if err != nil {
		return err
	}

	var query = util.ToSnakeCase(util.GetGormModelPrimaryKeyField(out)) + "= ?"
	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if tx.Error != nil {
		return tx.Error
	}
	tx = tx.Where(query, id)

	tx, err = juggler.FilterQuery(filterMap, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	if err := tx.First(out).Error; err != nil { // https://github.com/jinzhu/gorm/issues/857
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

// FindOne Repository
//		model	= Struct
// 		filter 	= Map
func (r *Repository) FindOne(out interface{}, filter interface{}) error {
	filterMap, err := getFilter(filter)
	if err != nil {
		return err
	}

	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	tx, err = juggler.FilterQuery(filterMap, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.First(out) // https://github.com/jinzhu/gorm/issues/857

	return tx.Commit().Error
}

// FindWithWhere Repository
//		out		= Struct
// 		query 	= string
// 		params 	= ...string
func (r *Repository) FindWithWhere(out interface{}, query string, params []interface{}) error {
	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Where(query, params...).First(out).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

// Count Repository
//		model	= Struct
// 		filter 	= Map
func (r *Repository) Count(model interface{}, filter interface{}) (int64, error) {
	var count int64

	filterMap, err := getFilter(filter)
	if err != nil {
		return 0, err
	}

	// Get rid of limit and ofset filter when counting
	filterMap.Limit = nil
	filterMap.Offset = nil
	filterMap.Include = nil
	filterMap.Fields = nil
	filterMap.Order = nil

	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if tx.Error != nil {
		return 0, tx.Error
	}

	tx.Model(model).Count(&count) // https://github.com/jinzhu/gorm/issues/857

	return count, tx.Commit().Error
}

// Upsert Repository
//		model	= Struct
// 		where 	= Map
// 		filter 	= Map
func (r *Repository) Upsert(model interface{}, where, filter interface{}) error {
	filterMap, err := getFilter(filter)
	if err != nil {
		return err
	}

	// Get rid of limit and ofset filter when updating
	filterMap.Limit = nil
	filterMap.Offset = nil
	filterMap.Include = nil
	filterMap.Fields = nil
	filterMap.Order = nil

	if filterMap.Where == nil {
		return fmt.Errorf("A filter with where clause is required to call update")
	}

	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	tx, err = juggler.FilterQuery(filterMap, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	if tx.Error != nil {
		return tx.Error
	}

	// DB.Model(&page).Updates(map[string]interface{}{"CommentsEnabled": commentsenabled})
	if err = tx.Where(where).Assign(util.ExtractMapInterface(model)).FirstOrCreate(model).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

// UpsertAll Repository
//		model		= Struct
// 		where 		= Map
// 		newvalues 	= Map
func (r *Repository) UpsertAll(model, where, newvalues interface{}) (bool, error) {
	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if tx.Error != nil {
		return false, tx.Error
	}

	// DB.Model(&page).Updates(map[string]interface{}{"CommentsEnabled": commentsenabled})
	// if err = tx.Where(model).Assign(util.ExtractMapInterface(model)).FirstOrCreate(model).Error; err != nil {
	if err := tx.Where(where).Assign(util.ExtractMapInterface(newvalues)).FirstOrCreate(model).Error; err != nil {
		tx.Rollback()
		return false, err
	}

	return true, tx.Commit().Error
}

// UpdateAll Repository
//		model	= Struct
// 		where 	= Map
// 		filter 	= Map
func (r *Repository) UpdateAll(model interface{}, where, filter interface{}) (bool, error) {
	filterMap, err := getFilter(filter)
	if err != nil {
		return false, err
	}

	// Get rid of limit and ofset filter when updating
	filterMap.Limit = nil
	filterMap.Offset = nil
	filterMap.Include = nil
	filterMap.Fields = nil
	filterMap.Order = nil

	if filterMap.Where == nil {
		return false, fmt.Errorf("A filter with where clause is required to call update")
	}

	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	tx, err = juggler.FilterQuery(filterMap, tx)
	if err != nil {
		tx.Rollback()
		return false, err
	}

	if tx.Error != nil {
		return false, tx.Error
	}

	// DB.Model(&page).Updates(map[string]interface{}{"CommentsEnabled": commentsenabled})
	if err = tx.Where(where).Model(model).Updates(util.ExtractMapInterface(model)).Error; err != nil {
		tx.Rollback()
		return false, err
	}

	return true, tx.Commit().Error
}

// DeleteByID Repository
//		model				= Struct
// 		deletePermanently 	= bool
func (r *Repository) DeleteByID(model interface{}, deletePermanently bool) (bool, error) {

	if deletePermanently {
		if err := r.GormDB.Unscoped().Delete(model).Error; err != nil {
			return false, err
		}
	} else {
		if err := r.GormDB.Delete(model).Error; err != nil {
			return false, err
		}
	}

	return true, nil
}

// DeleteAll Repository
//		model				= Struct
// 		where 				= Map
// 		filter 				= Map
// 		deletePermanently 	= bool
func (r *Repository) DeleteAll(model interface{}, where, filter interface{}, deletePermanently bool) (bool, error) {
	filterMap, err := getFilter(filter)
	if err != nil {
		return false, err
	}

	// Get rid of limit and ofset filter when updating
	filterMap.Limit = nil
	filterMap.Offset = nil
	filterMap.Include = nil
	filterMap.Fields = nil
	filterMap.Order = nil

	if filterMap.Where == nil {
		return false, fmt.Errorf("A filter with where clause is required to call delete")
	}

	tx := r.GormDB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if tx.Error != nil {
		return false, tx.Error
	}

	tx, err = juggler.FilterQuery(filterMap, tx)
	if err != nil {
		return false, err
	}

	// db.Unscoped().Delete(&order)

	if deletePermanently {
		if err = tx.Where(where).Unscoped().Delete(model).Error; err != nil {
			tx.Rollback()
			return false, err
		}
	} else {
		if err = tx.Where(where).Delete(model).Error; err != nil {
			tx.Rollback()
			return false, err
		}
	}

	return true, tx.Commit().Error
}
