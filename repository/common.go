package repository

import juggler "github.com/bekinsoft/ds-juggler"

func getFilter(filter interface{}) (filterMap juggler.Filter, err error) {
	if filter == nil {
		return
	}

	if f, ok := filter.(string); ok {
		filterMap, err = juggler.GetFilterParamMapFromJSONString(f)
		if err != nil {
			return
		}
	} else {
		filterMap = filter.(juggler.Filter)
	}

	return
}
