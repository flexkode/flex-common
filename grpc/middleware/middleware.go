package middleware

import (
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	ot "github.com/opentracing/opentracing-go"
	"gitlab.com/flexkode/flex-common/grpc/middleware/interceptors"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

// RegisterClientMiddlewares gets all unary and stream interceptors
func RegisterClientMiddlewares(opts []grpc.DialOption) []grpc.DialOption {

	retryUnaryInterceptor, retryStreamInterceptor := interceptors.RetryInterceptor()

	// Add unary interceptor
	opts = append(opts, grpc.WithUnaryInterceptor(retryUnaryInterceptor))
	opts = append(opts, grpc.WithStreamInterceptor(retryStreamInterceptor))

	opts = append(opts, grpc.WithStreamInterceptor(
		grpc_opentracing.StreamClientInterceptor(
			grpc_opentracing.WithTracer(ot.GlobalTracer()))))
	opts = append(opts, grpc.WithUnaryInterceptor(
		grpc_opentracing.UnaryClientInterceptor(
			grpc_opentracing.WithTracer(ot.GlobalTracer()))))

	return opts
}

// RegisterServerMiddlewares gets all unary and stream interceptors
func RegisterServerMiddlewares(logger *zap.Logger, opts []grpc.ServerOption) []grpc.ServerOption {

	loggerUnaryInterceptors, loggerStreamInterceptors := interceptors.LoggingInterceptors(logger, opts)
	recoveryUnaryInterceptors, recoveryStreamInterceptors := interceptors.RecoveryInterceptors(opts) // turn panics into gRPC errors

	var unary []grpc.UnaryServerInterceptor
	unary = append(unary, loggerUnaryInterceptors...)
	unary = append(unary, recoveryUnaryInterceptors...)

	var stream []grpc.StreamServerInterceptor
	stream = append(stream, loggerStreamInterceptors...)
	stream = append(stream, recoveryStreamInterceptors...)

	// Add unary interceptor
	opts = append(opts, grpc_middleware.WithUnaryServerChain(unary...))
	opts = append(opts, grpc_middleware.WithStreamServerChain(stream...))

	return opts
}
