package interceptors

import (
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc"
)

var (
	customFunc grpc_recovery.RecoveryHandlerFunc
)

// RecoveryInterceptors returns grpc.Server config option that turn on recovery.
func RecoveryInterceptors(opts []grpc.ServerOption) ([]grpc.UnaryServerInterceptor, []grpc.StreamServerInterceptor) {
	// Shared options for the logger, with a custom gRPC code to log level function.
	o := []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandler(customFunc),
	}

	var ui []grpc.UnaryServerInterceptor
	ui = append(ui, grpc_recovery.UnaryServerInterceptor(o...))

	// Add stream interceptor (added as an example here)
	var si []grpc.StreamServerInterceptor
	si = append(si, grpc_recovery.StreamServerInterceptor(o...))

	return ui, si
}
