package interceptors

import (
	"time"

	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// codeToLevel redirects OK to DEBUG level logging instead of INFO
// This is example how you can log several gRPC code results
func codeToLevel(code codes.Code) zapcore.Level {
	if code == codes.OK {
		// It is DEBUG
		return zap.DebugLevel
	}
	return grpc_zap.DefaultCodeToLevel(code)
}

func durationToLevel(duration time.Duration) zapcore.Field {
	return zap.String("grpc.time", duration.String())
}

// LoggingInterceptors returns grpc.Server config option that turn on logging.
func LoggingInterceptors(logger *zap.Logger, opts []grpc.ServerOption) ([]grpc.UnaryServerInterceptor, []grpc.StreamServerInterceptor) {
	// Shared options for the logger, with a custom gRPC code to log level function.
	o := []grpc_zap.Option{
		grpc_zap.WithDurationField(durationToLevel),
		grpc_zap.WithLevels(codeToLevel),
	}
	// Make sure that log statements internal to gRPC library are logged using the zapLogger as well.
	grpc_zap.ReplaceGrpcLoggerV2(logger)

	// Add unary interceptor
	var ui []grpc.UnaryServerInterceptor
	ui = append(ui, grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)))
	ui = append(ui, grpc_zap.UnaryServerInterceptor(logger, o...))

	// Add stream interceptor (added as an example here)
	var si []grpc.StreamServerInterceptor
	si = append(si, grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)))
	si = append(si, grpc_zap.StreamServerInterceptor(logger, o...))

	return ui, si
}
