package interceptors

import (
	"time"

	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"google.golang.org/grpc"
)

// ExponentBase2 computes 2^(a-1) where a >= 1. If a is 0, the result is 0.
func exponentBase2(a uint) uint {
	return (1 << a) >> 1
}

func backoffExponential(scalar time.Duration) grpc_retry.BackoffFunc {
	return func(attempt uint) time.Duration {
		return scalar * time.Duration(exponentBase2(attempt))
	}
}

// RetryInterceptor returns grpc.Server config option that turn on recovery.
func RetryInterceptor() (grpc.UnaryClientInterceptor, grpc.StreamClientInterceptor) {
	// Shared options for the logger, with a custom gRPC code to log level function.
	o := []grpc_retry.CallOption{
		grpc_retry.WithBackoff(backoffExponential(100 * time.Millisecond)),
		// grpc_retry.WithBackoff(grpc_retry.BackoffLinear(100 * time.Millisecond)),
		// grpc_retry.WithCodes(codes.NotFound, codes.Aborted), // retry only on NotFound and Unavailable
	}

	ui := grpc_retry.UnaryClientInterceptor(o...)

	// Add stream interceptor (added as an example here)
	si := grpc_retry.StreamClientInterceptor(o...)

	return ui, si
}
