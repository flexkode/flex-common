module gitlab.com/flexkode/flex-common

go 1.12

require (
	github.com/Masterminds/semver v1.5.0
	github.com/bekinsoft/ds-juggler v1.0.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.1.0
	github.com/idealitsol/beacon-proto v0.2.14
	github.com/idealitsol/beacon-util v0.0.23
	github.com/jinzhu/gorm v1.9.10
	github.com/opentracing/opentracing-go v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/viper v1.6.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.13.0
	google.golang.org/grpc v1.21.0
	gopkg.in/h2non/gock.v1 v1.0.15
)
